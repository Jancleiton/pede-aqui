import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { cidades } from '../../shared/cidades';
import { categorias } from '../../shared/categorias';
import { Empresa, EmpresaService } from '../shared';

@Component({
  selector: 'app-lista-empresas',
  templateUrl: './lista-empresas.component.html',
  styleUrls: ['./lista-empresas.component.css']
})
export class ListaEmpresasComponent implements OnInit {  

  cidadesRN: Array<string> = []
  categorias: Array<string> = []
  empresas: Empresa[] = []
  pesquisou: boolean = false
  cidadePesquisada: string
  categoriaPesquisada: string  
  carregando: boolean = false

  formulario: FormGroup = new FormGroup({
    //cidade: new FormControl(null, Validators.required),
    categoria: new FormControl(null, Validators.required)    
  })

  constructor(private empresaDB: EmpresaService) {
    this.cidadesRN = cidades
    this.categorias = categorias
   }

  ngOnInit() {
  }

  listarEmpresas(){
    this.pesquisou = false
    this.carregando = true
    /* this.cidadePesquisada = this.formulario.get('cidade').value */
    this.categoriaPesquisada = categorias[this.formulario.get('categoria').value]
    this.empresaDB.pesquisarEmpresas("Santa Cruz", this.categoriaPesquisada).then(result => {
      this.empresas = result
      this.carregando = false
      this.pesquisou = true
      this.limparForm()
    })    
  }

  limparForm(){
    this.formulario.get('categoria').reset()    
  }

  exibirDetalhesEmpresas(i: number){
    console.log(i)
  }

}
