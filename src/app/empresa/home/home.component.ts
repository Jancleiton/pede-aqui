import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { cidades } from '../../shared/cidades';
import { categorias } from '../../shared/categorias';
import { UsuarioService } from '../../usuarios/services/usuario.service';
import { AlertaService } from '../../shared/alertaServico/alerta.service';
import { Empresa, EmpresaService, Endereco } from '../shared';
import { Usuario, AuthService } from 'src/app/usuarios';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  usuarioAtivo: Usuario
  empresaCadastrada: Empresa
  carregando: boolean = true
  apresentarFormulario: boolean = false
  cidadesRN: Array<string> = []
  categorias: Array<string> = []   

  formulario: FormGroup = new FormGroup({
    nomeEstabelecimento: new FormControl(null, Validators.required),
    /* cidade: new FormControl(null, Validators.required), */
    categoria: new FormControl(null, Validators.required),
    frete: new FormControl(null, Validators.required),
    endereco: new FormControl(null, Validators.required),
    numero: new FormControl(null, Validators.required),    
    bairro: new FormControl(null, Validators.required),
    whatsapp: new FormControl(null, Validators.required),
    instagram: new FormControl(null, Validators.nullValidator),
    linkContato: new FormControl(null, Validators.nullValidator)
  })

  constructor(
    private usuarioDB: UsuarioService,
    private empresaDB: EmpresaService,
    private auth: AuthService,
    private alerta: AlertaService
  ) {
    this.cidadesRN = cidades
    this.categorias = categorias    
   }

  ngOnInit() {    
    this.carregarUsuarioAtivo()       
  }

  carregarUsuarioAtivo(){    
    this.usuarioDB.recuperarUsuarioAtivo(localStorage.getItem('idCliente')).then(result => {      
      this.usuarioAtivo = result      
      if(this.usuarioAtivo.cidadeEmpresaCadastrada)
        this.carregarEmpresaCadastrada()              
      else{
        this.apresentarFormulario = true
        this.carregando = false
      }        
    })
  }

  cadastrarEmpresa(){
    if(this.usuarioAtivo.cidadeEmpresaCadastrada){
      this.efetuarEdicao()
    }else{
      this.efetuarCadastro()      
    }              
  }

  preencheEmpresaComDadosFormulario(): Empresa{
    let empresa = new Empresa()
    empresa.endereco = new Endereco()    
    empresa.idUsuarioResponsavel = this.usuarioAtivo.uid
    empresa.nomeEstabelecimento = this.formulario.get('nomeEstabelecimento').value    
    empresa.cidade = "Santa Cruz" // FIXO PARA SANTA CRUZ
    empresa.categoria = this.categorias[this.formulario.get('categoria').value]
    empresa.frete = this.formulario.get('frete').value
    empresa.endereco.endereco = this.formulario.get('endereco').value
    empresa.endereco.numero = this.formulario.get('numero').value
    empresa.endereco.bairro = this.formulario.get('bairro').value
    empresa.whatsapp = this.formulario.get('whatsapp').value
    empresa.instagram = this.formulario.get('instagram').value
    empresa.linkContato = this.formulario.get('linkContato').value
    return empresa
  }
  
  efetuarCadastro(){
    let empresa: Empresa = this.preencheEmpresaComDadosFormulario()            

    this.empresaDB.cadastrarEmpresa(empresa).then(cadastrou => {
      if(cadastrou){
        this.usuarioAtivo.cidadeEmpresaCadastrada = empresa.cidade
        this.usuarioAtivo.categoriaEmpresaCadastrada = empresa.categoria
        this.usuarioDB.atualizar(this.usuarioAtivo).then(atualizou => {
          if(atualizou){
            this.carregarEmpresaCadastrada()
            this.alerta.mensagem.next('Empresa cadastrada!')            
          }else{
            this.empresaDB.excluirEmpresa(this.usuarioAtivo)
            this.alerta.mensagem.next('Falha ao cadastrar, verifique a conexão e tente novamente!')
          }            
        })          
      }
    })    
  }

  efetuarEdicao(){    
    let empresa: Empresa = this.preencheEmpresaComDadosFormulario()
    
    if(this.empresaCadastrada.logo)
      empresa.logo = this.empresaCadastrada.logo

    if(this.usuarioAtivo.cidadeEmpresaCadastrada == empresa.cidade && this.usuarioAtivo.categoriaEmpresaCadastrada == empresa.categoria){      
      this.empresaDB.atualizarEmpresa(empresa).then(atualizou => {
        atualizou ? this.alerta.mensagem.next('Dados atualizados!') : this.alerta.mensagem.next('Falha ao atualizar, verifique a conexão e tente novamente!')
        this.carregarEmpresaCadastrada()        
      })
    }else{
      //excluindo registro antigo
      this.empresaDB.excluirEmpresa(this.usuarioAtivo)
      //atualiza dados de cidade e categoria em usuario
      this.usuarioAtivo.cidadeEmpresaCadastrada = empresa.cidade
      this.usuarioAtivo.categoriaEmpresaCadastrada = empresa.categoria
      this.usuarioDB.atualizaUsuario(this.usuarioAtivo)
      //cadastrando novo registro
      this.empresaDB.cadastrarEmpresa(empresa).then( cadastrou => {
        if(cadastrou){
          this.carregarEmpresaCadastrada()
          this.alerta.mensagem.next('Dados atualizados!')
        } 
        else
          this.alerta.mensagem.next('Falha ao atualizar, verifique a conexão e tente novamente!') 
      })            
    }    
  }

  carregarEmpresaCadastrada(){      
    this.empresaDB.empresaCadastrada(this.usuarioAtivo).then(result => {    
    this.empresaCadastrada = result    
    this.apresentarFormulario = false 
    this.carregando = false   
    })
  }

  enviarDadosParaFormulario(){
    this.apresentarFormulario = true
    this.formulario.get('nomeEstabelecimento').setValue(this.empresaCadastrada.nomeEstabelecimento)
    /* this.formulario.get('cidade').setValue(this.empresaCadastrada.cidade) */   
    this.formulario.get('categoria').setValue(this.categorias.indexOf(this.empresaCadastrada.categoria))
    this.formulario.get('frete').setValue(this.empresaCadastrada.frete)
    this.formulario.get('endereco').setValue(this.empresaCadastrada.endereco.endereco)
    this.formulario.get('numero').setValue(this.empresaCadastrada.endereco.numero)
    this.formulario.get('bairro').setValue(this.empresaCadastrada.endereco.bairro)
    this.formulario.get('whatsapp').setValue(this.empresaCadastrada.whatsapp)
    this.formulario.get('instagram').setValue(this.empresaCadastrada.instagram)
    this.formulario.get('linkContato').setValue(this.empresaCadastrada.linkContato)   
  }

  sair(){
    this.auth.logout()
  }

}
