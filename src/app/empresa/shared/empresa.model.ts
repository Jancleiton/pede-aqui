import { Endereco } from './endereco.model'    

export class Empresa {

    idUsuarioResponsavel: string
    nomeEstabelecimento: string
    logo: string
    cidade: string
    categoria: string
    endereco: Endereco
    frete: string
    whatsapp: string
    instagram: string
    linkContato: string
}