import * as firebase from 'firebase/app'
import 'firebase/database'
import { Empresa } from './empresa.model'
import { Usuario } from '../../usuarios/models/usuario.model'
import { Injectable } from "@angular/core";

@Injectable()
export class EmpresaService {
  banco = firebase.database()

  cadastrarEmpresa(empresa: Empresa): Promise<boolean> {
    empresa = this.lowerCase(empresa)
    return new Promise((resolve, reject) => {
      this.banco
      .ref('empresas')
      .child(`${empresa.cidade}`)
      .child(`${empresa.categoria}`)
      .child(`${empresa.idUsuarioResponsavel}`)
      .set(empresa)
      resolve(true)
      reject(false)
    })    
    
  }

  lowerCase(empresa: Empresa): Empresa{    
    empresa.nomeEstabelecimento = empresa.nomeEstabelecimento.toLowerCase()
    if(empresa.endereco.endereco)
      empresa.endereco.endereco = empresa.endereco.endereco.toLowerCase()
    if(empresa.endereco.bairro)
      empresa.endereco.bairro = empresa.endereco.bairro.toLowerCase()
    if(empresa.instagram)
      empresa.instagram = empresa.instagram.toLowerCase()    
    return empresa
  }

  empresaCadastrada(usuario: Usuario): Promise<Empresa>{
    return new Promise(resolve => {
      this.banco.ref(`empresas`).child(usuario.cidadeEmpresaCadastrada).child(usuario.categoriaEmpresaCadastrada).once('value', snapshot => {                
        snapshot.forEach(childSnapshot => {          
          let empresa: Empresa = childSnapshot.val()
          if(empresa.idUsuarioResponsavel === usuario.uid)
            resolve(empresa)          
        })        
      })
    })
  }
  
  atualizarEmpresa(empresa: Empresa):Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.banco
        .ref('empresas')
        .child(empresa.cidade)
        .child(empresa.categoria)
        .child(empresa.idUsuarioResponsavel)
        .update(empresa)
      resolve(true)
      reject(false)
    })
}

excluirEmpresa(usuario: Usuario): Promise<boolean>{
  return new Promise((resolve, reject) => {
      firebase.database()
      .ref('empresas')
      .child(usuario.cidadeEmpresaCadastrada)
      .child(usuario.categoriaEmpresaCadastrada)
      .child(usuario.uid)
      .remove()
      resolve(true)
      reject(false)
  })                
}

  pesquisarEmpresas(cidade: string, categoria: string): Promise<Empresa[]>{
    return new Promise(resolve => {
      this.banco.ref(`empresas`).child(cidade).child(categoria).orderByChild('nomeEstabelecimento').once('value', snapshot => {        
        let empresas = new Array<Empresa>()
        snapshot.forEach(childSnapshot => {
          let empresa = childSnapshot.val()
          empresas.push(empresa)          
        })
        resolve(empresas)
      })
    })
  }
}