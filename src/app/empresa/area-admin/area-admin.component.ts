import { Component, OnInit } from '@angular/core';
import { cidades } from '../../shared/cidades';
import { categorias } from '../../shared/categorias';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertaService } from '../../shared/alertaServico/alerta.service';
import { EmpresaService, Empresa, Endereco } from '../shared';
import { AuthService } from 'src/app/usuarios';

@Component({
  selector: 'app-area-admin',
  templateUrl: './area-admin.component.html',
  styleUrls: ['./area-admin.component.css']
})
export class AreaAdminComponent implements OnInit {

  cidadesRN: Array<string> = []
  categorias: Array<string> = []

  formulario: FormGroup = new FormGroup({
    nomeEstabelecimento: new FormControl(null, Validators.required),
    /* cidade: new FormControl(null, Validators.required), */
    categoria: new FormControl(null, Validators.required),
    frete: new FormControl(null, Validators.required),
    endereco: new FormControl(null, Validators.nullValidator),
    numero: new FormControl(null, Validators.nullValidator),    
    bairro: new FormControl(null, Validators.nullValidator),
    whatsapp: new FormControl(null, Validators.required),
    instagram: new FormControl(null, Validators.nullValidator),
    linkContato: new FormControl(null, Validators.nullValidator)
  })

  constructor(    
    private empresaDB: EmpresaService,
    private auth: AuthService,
    private alerta: AlertaService
  ) {
    this.cidadesRN = cidades
    this.categorias = categorias
   }

  ngOnInit() {
  }

  efetuarCadastro(){
    let empresa: Empresa = this.preencheEmpresaComDadosFormulario()            

    this.empresaDB.cadastrarEmpresa(empresa).then(cadastrou => {
      if(cadastrou){
        this.alerta.mensagem.next('Empresa cadastrada!')
        this.resetarFormulario()
      }                
      else
        this.alerta.mensagem.next('Falha ao cadastrar, verifique a conexão e tente novamente!')      
    })    
  }

  resetarFormulario(){
    this.formulario.get('nomeEstabelecimento').reset()
    /* this.formulario.get('cidade').reset()    */
    this.formulario.get('categoria').reset()
    this.formulario.get('frete').reset()
    this.formulario.get('endereco').reset()
    this.formulario.get('numero').reset()
    this.formulario.get('bairro').reset()
    this.formulario.get('whatsapp').reset()
    this.formulario.get('instagram').reset()
    this.formulario.get('linkContato').reset()
  }

  preencheEmpresaComDadosFormulario(): Empresa{
    let empresa = new Empresa()
    empresa.endereco = new Endereco()        
    empresa.nomeEstabelecimento = this.formulario.get('nomeEstabelecimento').value    
    empresa.cidade = "Santa Cruz"
    empresa.categoria = this.categorias[this.formulario.get('categoria').value]
    empresa.frete = this.formulario.get('frete').value
    empresa.endereco.endereco = this.formulario.get('endereco').value
    empresa.endereco.numero = this.formulario.get('numero').value
    empresa.endereco.bairro = this.formulario.get('bairro').value
    empresa.whatsapp = this.formulario.get('whatsapp').value
    empresa.instagram = this.formulario.get('instagram').value
    empresa.linkContato = this.formulario.get('linkContato').value
    empresa.idUsuarioResponsavel = empresa.nomeEstabelecimento + empresa.endereco.endereco + empresa.endereco.numero + empresa.endereco.bairro
    return empresa
  }

  sair(){
    this.auth.logout()
  }
}
