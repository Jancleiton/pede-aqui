import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaEmpresasComponent } from './listar';
import { ReactiveFormsModule } from '@angular/forms';
import { EmpresaService } from './shared';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { SpinnerModule } from '../spinner';
import { InputMaskModule } from 'primeng/inputmask';
import { HomeComponent } from './home';
import { AreaAdminComponent } from './area-admin';
import { MeuComercioComponent } from './meu-comercio.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ListaEmpresasComponent,
    HomeComponent,
    AreaAdminComponent,
    MeuComercioComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    InputMaskModule,
    SpinnerModule
  ],  
  providers: [
    EmpresaService    
  ],
  exports: [    
  ]
})
export class EmpresaModule {
  constructor(libraryIcons: FaIconLibrary){
    libraryIcons.addIconPacks(fas, fab)    
  }
 }
