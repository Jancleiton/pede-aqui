import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-meu-comercio',
  template: '<router-outlet></router-outlet>',
})
export class MeuComercioComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
