import { Component } from '@angular/core';
import { AuthService } from 'src/app/usuarios';

@Component({
  selector: 'app-botao-login-facebook',
  templateUrl: './botao-login-facebook.component.html',
  styleUrls: ['./botao-login-facebook.component.css']
})
export class BotaoLoginFacebookComponent {
  constructor(public authService: AuthService) {}
}
