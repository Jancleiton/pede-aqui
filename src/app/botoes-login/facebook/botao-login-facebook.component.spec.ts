import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotaoLoginFacebookComponent } from './botao-login-facebook.component';

describe('BotaoLoginFacebookComponent', () => {
  let component: BotaoLoginFacebookComponent;
  let fixture: ComponentFixture<BotaoLoginFacebookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotaoLoginFacebookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotaoLoginFacebookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
