import { Component } from '@angular/core'
import { AuthService } from 'src/app/usuarios';

@Component({
  selector: 'app-botao-login-google',
  templateUrl: './botao-login-google.component.html',
  styleUrls: ['./botao-login-google.component.css'],
})
export class BotaoLoginGoogleComponent {
  constructor(public authService: AuthService) {}
}
