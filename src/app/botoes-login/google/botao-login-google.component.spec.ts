import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { BotaoLoginGoogleComponent } from './botao-login-google.component'

describe('BotaoLoginComponent', () => {
  let component: BotaoLoginGoogleComponent
  let fixture: ComponentFixture<BotaoLoginGoogleComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BotaoLoginGoogleComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(BotaoLoginGoogleComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
