import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BotaoLoginFacebookComponent } from './facebook';
import { BotaoLoginGoogleComponent } from './google';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { fab } from '@fortawesome/free-brands-svg-icons';

@NgModule({
  declarations: [
    BotaoLoginFacebookComponent,
    BotaoLoginGoogleComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    BotaoLoginFacebookComponent,
    BotaoLoginGoogleComponent
  ]
})
export class BotoesLoginModule { 
  constructor(libraryIcons: FaIconLibrary){
    libraryIcons.addIconPacks(fab)    
  }
}
