import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService, UsuarioService } from '../usuarios/services';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    AuthService,
    UsuarioService
  ]
})
export class UsuariosModule { }
