import * as firebase from 'firebase/app'
import 'firebase/database'
import { Router } from '@angular/router'
import { Injectable } from '@angular/core'
import { auth } from 'firebase/app'
import { AngularFireAuth } from '@angular/fire/auth'
import { Usuario } from '../models/usuario.model'
import { UsuarioService } from './usuario.service'

@Injectable()
export class AuthService {
  tokenID: string
  clienteID: string

  constructor(public router: Router, public usuarioDB: UsuarioService, public afAuth: AngularFireAuth) { }

  async authLogin(provider) {
    try {
      const resp = await this.afAuth.auth.signInWithPopup(provider)
      const usuario = new Usuario()

      usuario.uid = resp.user.uid
      usuario.nome = resp.user.displayName.toLowerCase()
      usuario.email = resp.user.email
      usuario.foto = resp.user.photoURL


      this.tokenID = resp.user.refreshToken
      this.clienteID = usuario.uid

      localStorage.setItem('tokenCliente', this.tokenID)
      localStorage.setItem('idCliente', usuario.uid)

      this.usuarioDB.verificarSeUsuarioExiste(usuario.uid).then((user: Usuario) => {
        if (user === null) {          
          this.usuarioDB.cadastrarUsuario(usuario)
          this.router.navigate(['meuComercio'])
        }else{
          if (user.admin != undefined)
            this.router.navigate(['meuComercio/areaAdmin'])
          else
            this.router.navigate(['meuComercio'])
        }
        
      })
    } catch (error) {
      alert(error)
      console.log(error)
    }
  }

  loginComFacebook() {
    return this.authLogin(new auth.FacebookAuthProvider())
  }

  loginComGoogleNovo() {
    return this.authLogin(new auth.GoogleAuthProvider())
  }

  autenticado(): boolean {
    this.usuarioDB.verificarSeUsuarioExiste(localStorage.getItem('idCliente')).then(c => {
      if (c == null) {
        this.logout()
        return false
      }
    })

    if (this.tokenID === undefined && localStorage.getItem('tokenCliente') !== null) {
      this.tokenID = localStorage.getItem('tokenCliente')
    }
    if (this.tokenID === undefined) {
      this.router.navigate([''])
    }

    return this.tokenID !== undefined
  }

  logout() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        localStorage.removeItem('tokenCliente')
        localStorage.removeItem('idCliente')
        this.tokenID = undefined
        this.clienteID = undefined
        this.router.navigate(['login'])
      })
  }
}
