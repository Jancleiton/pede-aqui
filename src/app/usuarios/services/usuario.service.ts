import * as firebase from 'firebase/app'
import 'firebase/database'
import { Observable, Observer } from 'rxjs'
import { Usuario } from '../models/usuario.model'
import { Injectable } from "@angular/core";

@Injectable()
export class UsuarioService {
  banco = firebase.database()

  cadastrarUsuario(usuario: Usuario) {
    this.banco
      .ref('usuarios')
      .child(`${usuario.uid}`)
      .set(usuario)
  }

  recuperarUsuarioAtivo(id: string): Promise<Usuario> {
    return new Promise(resolve => {
      this.banco.ref(`usuarios/${id}`).once('value', snapshot => {
        let usuario = snapshot.val()        
        resolve(usuario)
      })
    })
  }

  verificarSeUsuarioExiste(usuarioID: string): Promise<Usuario> {
    return new Promise(resolve =>
      this.banco.ref(`usuarios/${usuarioID}`).once('value', snapshot => resolve(snapshot.val())),
    )
  }

  atualizaUsuario(usuario: Usuario) {
    this.banco
      .ref('usuarios')
      .child(usuario.uid)
      .update(usuario)
  }

  ouvirUsuarioAtivo(id: string): Observable<Usuario> {
    let observer = Observable.create((observer: Observer<Usuario>) => {
      this.banco.ref(`usuarios/${id}`).on('value', snapshot => {
        let usuario = new Usuario()
        usuario = snapshot.val()
        observer.next(usuario)
      })
    })
    return observer
  }

  atualizar(usuario: Usuario): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.banco
        .ref('usuarios')
        .child(usuario.uid)
        .update(usuario)
      resolve(true)
      reject(false)
    })
  }
}
