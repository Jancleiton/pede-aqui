export class Usuario {
    uid: string
    nome: string
    email: string
    foto: string
    telefone: string
    cidadeEmpresaCadastrada: string
    categoriaEmpresaCadastrada: string
    admin: boolean
}