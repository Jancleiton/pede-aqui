import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import localePt from '@angular/common/locales/pt'
import { registerLocaleData } from '@angular/common'

import { AppRoutingModule } from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SidebarModule } from 'primeng/sidebar'
import { ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './app.component';

import { TopoComponent } from './topo/topo.component';
import { LoginComponent } from './login/login.component';
import { SobreComponent } from './sobre/sobre.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore'
import { environment } from 'src/environments/environment.prod';
import { RotaGuarda } from './shared/rotaGuarda';
import { AlertaServicoComponent } from './shared/alertaServico/alerta-servico/alerta-servico.component';
import { AlertaService } from './shared/alertaServico/alerta.service';
import { InstrucaoLogoComponent } from './instrucao-logo/instrucao-logo.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { BotoesLoginModule } from './botoes-login';
import { EmpresaModule } from './empresa';
import { SpinnerModule } from './spinner';
import { UsuariosModule } from './usuarios';

@NgModule({
  declarations: [
    AppComponent,
    TopoComponent,    
    LoginComponent,        
    SobreComponent,           
    AlertaServicoComponent,
    InstrucaoLogoComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,    
    BrowserAnimationsModule,
    SidebarModule,    
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    FontAwesomeModule,
    BotoesLoginModule,
    EmpresaModule,
    SpinnerModule,
    UsuariosModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt' },    
    RotaGuarda,
    AlertaService,          
    AngularFireModule,
    AngularFireAuthModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(libraryIcons: FaIconLibrary){
    libraryIcons.addIconPacks(fas, fab)    
  }
 }

registerLocaleData(localePt)