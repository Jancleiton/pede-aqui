import { CanActivate } from '@angular/router'
import { Injectable } from '@angular/core'
import { AuthService } from '../usuarios'

@Injectable()
export class RotaGuarda implements CanActivate {
  constructor(private auth: AuthService) {}

  canActivate(): boolean {
    return this.auth.autenticado()
  }
}
