import { Component, OnInit } from '@angular/core';
import { AlertaService } from '../alerta.service';

@Component({
  selector: 'app-alerta-servico',
  templateUrl: './alerta-servico.component.html',
  styleUrls: ['./alerta-servico.component.css']
})
export class AlertaServicoComponent implements OnInit {

  mensagem: string
  display: string = 'none'

  constructor(private alerta: AlertaService) { }

  ngOnInit() {
    this.ouvirMensagem()
  }

  ouvirMensagem(){
    this.alerta.mensagem.subscribe(msg => {
      if(msg){
        this.mensagem = msg
      this.display = 'block'
      }      
    })
  }

  fechar(){
    this.display = 'none'
  }

}
