import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../usuarios/services/usuario.service';
import { Usuario } from '../usuarios';

@Component({
  selector: 'app-topo',
  templateUrl: './topo.component.html',
  styleUrls: ['./topo.component.css']
})
export class TopoComponent implements OnInit {

  menu: boolean
  usuarioAtivo: Usuario

  constructor(
    private router: Router,
    private usuarioDB: UsuarioService
    ) { }

  ngOnInit() {
  }

  meuComercio(){    
    localStorage.getItem('idCliente')? this.verificaRotaAdmin() :  this.router.navigate(['login'])            
  }

  verificaRotaAdmin(){
    this.usuarioDB.recuperarUsuarioAtivo(localStorage.getItem('idCliente')).then(result => {      
      this.usuarioAtivo = result      
      if(this.usuarioAtivo != null){
        if(this.usuarioAtivo.admin){  
          this.router.navigate(['meuComercio/areaAdmin'])    
        }else{
          this.router.navigate(['meuComercio'])
        }
      }                                   
    })
  }

}
