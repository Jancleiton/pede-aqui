import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaEmpresasComponent, HomeComponent } from './empresa';
import { LoginComponent } from './login/login.component';
import { SobreComponent } from './sobre/sobre.component';
import { RotaGuarda } from './shared/rotaGuarda';
import { InstrucaoLogoComponent } from './instrucao-logo/instrucao-logo.component';
import { AreaAdminComponent } from './empresa/area-admin/area-admin.component';
import { MeuComercioComponent } from './empresa';


const routes: Routes = [
  { path: '', component: ListaEmpresasComponent },
  { path: 'login', component: LoginComponent },
  { path: 'sobre', component: SobreComponent },
  { path: 'instrucaoLogo', component: InstrucaoLogoComponent },
  { path: 'meuComercio', component: MeuComercioComponent, canActivate: [RotaGuarda],
    children: [
      { path: '', component: HomeComponent },
      { path: 'areaAdmin', component: AreaAdminComponent }
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
